import { Component, Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class Todo1Service {
  constructor(private http: HttpClient) {
  }

  getTodos(): Observable<any> {
    return this.http.get<any>('data/todo.json');
  }
}

@Component({
  selector: 'app-todo',
  providers: [Todo1Service],
  template: `<h1>Attempt 1</h1>
    <div *ngFor="let todo of todos1">{{todo.id}}. {{todo.text}}</div>
    <h2>Attempt 2</h2>
    <div *ngFor="let todo of todos2 | async">{{todo.id}}. {{todo.text}}</div>
  `
})
export class AppTodo1Component implements OnInit {
  todos1: any[];
  todos2: Observable<any[]>;

  constructor(private todoService: Todo1Service) {
  }

  ngOnInit() {
    this.todoService.getTodos().subscribe(todos => this.todos1 = todos);
    this.todos2 = this.todoService.getTodos();
  }
}
