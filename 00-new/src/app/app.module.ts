import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppGamesComponent } from './games';
import { AppHelloWorldComponent} from './1-component';
import { AppShoppingCartComponent} from './1-component';
import { AppClickCounterComponent} from './1-component';
import { AppTodoComponent} from './1-component';
import { AppLoginComponent, AppTodoItemComponent } from './1-component';
import { AppGameOfLifeComponent, MyBoardPipe } from './5-tdd';
import { AppLeaderboard1Component, AppLeaderboardIndex2Component } from './2-service';
import { AppLeaderboardIndex3Component, AppLeaderboard2Component, AppLeaderboard3Component } from './2-service';
import { AppDirectiveShowcaseComponent, AppHelloWorldDirective } from './3-directive';
import { AppTodo1Component} from './4-promise';

import { GameManagementService } from './games/gameManagement.service';
import { Rpc } from './shared/rpc';
import { ToggleService, AppToggleDirective, AppToggleableDirective } from './shared/toggle';

@NgModule({
  declarations: [
    AppComponent,
    AppGamesComponent,
    AppHelloWorldComponent,
    AppShoppingCartComponent,
    AppClickCounterComponent,
    AppTodoComponent,
    AppTodoItemComponent,
    AppLoginComponent,
    AppGameOfLifeComponent,
    AppLeaderboard1Component,
    AppLeaderboard2Component,
    AppLeaderboard3Component,
    AppLeaderboardIndex2Component,
    AppLeaderboardIndex3Component,
    AppDirectiveShowcaseComponent,
    AppTodo1Component,
    AppHelloWorldDirective,
    AppToggleDirective,
    AppToggleableDirective,
    MyBoardPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    GameManagementService,
    Rpc,
    ToggleService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
