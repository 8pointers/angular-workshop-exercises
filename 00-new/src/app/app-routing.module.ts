import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppGamesComponent } from './games';
import { AppHelloWorldComponent} from './1-component';
import { AppShoppingCartComponent} from './1-component';
import { AppClickCounterComponent} from './1-component';
import { AppTodoComponent} from './1-component';
import { AppLoginComponent} from './1-component';
import { AppGameOfLifeComponent} from './5-tdd';

import { AppLeaderboard1Component, AppLeaderboardIndex2Component, AppLeaderboardIndex3Component } from './2-service';

import { AppDirectiveShowcaseComponent} from './3-directive';

import { AppTodo1Component} from './4-promise';

const routes: Routes = [
  { path: '', component: AppHelloWorldComponent },

  { path: 'helloWorld', component: AppHelloWorldComponent},
  { path: 'shoppingCart', component: AppShoppingCartComponent},
  { path: 'clickCounter', component: AppClickCounterComponent},
  { path: 'todo', component: AppTodoComponent},
  { path: 'login', component: AppLoginComponent},

  { path: 'leaderboard1', component: AppLeaderboard1Component},
  { path: 'leaderboard2', component: AppLeaderboardIndex2Component},
  { path: 'leaderboard3', component: AppLeaderboardIndex3Component},

  { path: 'directive', component: AppDirectiveShowcaseComponent},

  { path: 'todo2', component: AppTodo1Component},
  { path: 'gameOfLife', component: AppGameOfLifeComponent},

  { path: 'games', component: AppGamesComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
