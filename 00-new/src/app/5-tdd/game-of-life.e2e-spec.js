describe('Game of Life', function () {
  beforeEach(function () {
    browser.get('/gameOfLife');
  });

  it('should have Next button', function () {
    const next = element(by.css('.tick'));
    expect(next.isPresent()).toEqual(true);
    expect(next.getText()).toEqual('Next');
  });
});
