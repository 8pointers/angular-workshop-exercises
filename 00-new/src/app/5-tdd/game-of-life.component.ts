import { Component, Injectable, OnInit, Pipe, PipeTransform } from '@angular/core';

@Injectable()
export class GameOfLife {
  public isAlive = {};

  private cellKey(row, column) {
    return row + '_' + column;
  }

  public isCellAlive (row, column) {
    return this.isAlive[this.cellKey(row, column)] || false;
  }

  public toggleCellState (row, column) {
    const key = this.cellKey(row, column);
    if (this.isAlive[key]) {
      delete this.isAlive[key];
    } else {
      this.isAlive[key] = true;
    }
    return this;
  }

  public tick () {
    const numberOfNeighbours = {};
    let parts, row, column, neighbourKey;
    for (const key in this.isAlive) {
      if (this.isAlive.hasOwnProperty(key)) {
        parts = key.split('_');
        row = parseInt(parts[0], 10);
        column = parseInt(parts[1], 10);
        numberOfNeighbours[key] = numberOfNeighbours[key] || 0;
        [[-1, -1], [-1, 0], [-1, 1], [0, -1], [0, 1], [1, -1], [1, 0], [1, 1]].forEach(offset => {
          neighbourKey = this.cellKey(row + offset[0], column + offset[1]);
          numberOfNeighbours[neighbourKey] = (numberOfNeighbours[neighbourKey] || 0) + 1;
        });
      }
    }
    for (const key in numberOfNeighbours) {
      if (numberOfNeighbours.hasOwnProperty(key)) {
        const shouldDie = this.isAlive[key] && (numberOfNeighbours[key] < 2 || numberOfNeighbours[key] > 3);
        const shouldResurrect = !this.isAlive[key] && numberOfNeighbours[key] === 3;
        if (shouldDie || shouldResurrect) {
          parts = key.split('_');
          row = parseInt(parts[0], 10);
          column = parseInt(parts[1], 10);
          this.toggleCellState(row, column);
        }
      }
    }
  };
}

@Pipe({
  name: 'myBoard',
  pure: false
})
export class MyBoardPipe implements PipeTransform {
  transform(value): any {
    return Array.apply(null, {length: 100})
      .map(Number.call, Number)
      .map(x => {
        return {
          row: x % 10,
          column: Math.floor(x / 10),
          isAlive: value['' + (x % 10) + '_' + Math.floor(x / 10)]
        };
      });
  }
}
@Component({
  providers: [GameOfLife],
  selector: 'app-game-of-life',
  styleUrls: ['./game-of-life.component.css'],
  template: `<div>
    <div class="grid">
      <div *ngFor="let cell of isAlive | myBoard"
        class="cell"
        [ngClass]="{alive: cell.isAlive}"
        [ngStyle]="{top: 20 * cell.row + 'px', left: 20 * cell.column + 'px'}"
        (click)="toggleCellState(cell.row, cell.column)">
      </div>
    </div>
    <button class="tick" (click)="tick()">Next</button>
  </div>`
})
export class AppGameOfLifeComponent implements OnInit {
  public isAlive: any;

  constructor(private gameOfLife: GameOfLife) {
    this.isAlive = gameOfLife.isAlive;
  }

  toggleCellState(row, column) {
    this.gameOfLife.toggleCellState(row, column);
  }

  tick() {
    this.gameOfLife.tick();
  }

  ngOnInit() {
  }
}
