import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-click-counter',
  templateUrl: './5-click-counter.component.html'
})
export class AppClickCounterComponent implements OnInit {
  times: number;

  count(): void {
    this.times++;
  }

  ngOnInit() {
    this.times = 0;
  }
}
