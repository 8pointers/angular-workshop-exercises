import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hello-world',
  template: '<div>Hello {{name}}!</div>'
})
export class AppHelloWorldComponent implements OnInit {
  name: string;

  ngOnInit() {
    this.name = 'World';
  }
}
