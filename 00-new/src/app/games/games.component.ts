import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { GameManagementService } from './gameManagement.service';

@Component({
  selector: 'app-games',
  providers: [GameManagementService],
  templateUrl: './games.html',
})
export class AppGamesComponent implements OnInit {
  games: Observable<any>;

  constructor(private gameManagementService: GameManagementService) {
  }

  suspendGame(gameId: string, isSuspended: boolean) {
    this.gameManagementService.suspendGame(gameId, false).subscribe();
  }

  ngOnInit() {
    this.games = this.gameManagementService.getAllGames();
  }
}
