import { Component, OnInit } from '@angular/core';
import { AppHelloWorldDirective } from './1-hello-world.directive';

@Component({
  selector: 'app-directive-showcase',
  templateUrl: './directive-showcase.component.html',
  styles: ['.hidden { display: none }']
})
export class AppDirectiveShowcaseComponent implements OnInit {
  ngOnInit() {
  }
}
