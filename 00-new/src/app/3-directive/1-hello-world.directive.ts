import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appHelloWorld]'
})
export class AppHelloWorldDirective {
  @Input('myHelloWorld')
  myHelloWorld: string;

  constructor(el: ElementRef) {
  }

  @HostListener('click')
  greet() {
    alert(this.myHelloWorld);
  }
}
