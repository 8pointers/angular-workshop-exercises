/*

  Your task is to implement and wire 2 directives that can be used to toggle a class

  <button appToggle="details">

  <div appToggleable="details" appToggleableClass="hidden">
    Now you see me
  </div>

*/
