import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-todo-item',
  template: `<div (click)="toggle()" [ngClass]="{done: item.done}">{{item.text}}</div>`,
  styles: [`
    .done {
      text-decoration: line-through
    }
  `]
})
export class TodoItemComponent {
  @Input()
  item: any;

  @Output()
  itemToggled = new EventEmitter();

  toggle() {
    this.itemToggled.next(this.item);
  }
}

@Component({
  selector: 'app-new-todo',
  template: `
  <form (ngSubmit)="add(); todoForm.reset()" #todoForm="ngForm">
    <div class="form-group">
      <label for="todo">Todo</label>
      <input type="text" class="form-control" required [(ngModel)]="model.todo" name="todo" #todoSpy #todo="ngModel">
      {{todoSpy.value}}:{{todoSpy.className}}
      <div [hidden]="todo.valid || todo.pristine" class="alert alert-danger">Required</div>
    </div>
    <button type="submit" class="btn btn-default" [disabled]="!todoForm.form.valid">
      Login
    </button>
  </form>`
})
export class NewTodoComponent {
  model = {
    todo: ''
  };

  @Output()
  itemAdded = new EventEmitter();

  add() {
    this.itemAdded.next(this.model.todo);
  }
}

@Component({
  selector: 'app-todo',
  template: `
    <app-new-todo (itemAdded)="add($event)"></app-new-todo>
    <app-todo-item *ngFor="let item of items" [item]="item" (itemToggled)="toggle(item)"></app-todo-item>
  `
})
export class TodoComponent implements OnInit {
  counter = 6;
  items: any[];

  add(todo) {
    this.items.push({id: this.counter++, text: todo});
  }

  toggle(item) {
    console.log('done');
    item.done = !item.done;
  }

  ngOnInit() {
    this.items = [
      { id: 0, text: 'Do this first' },
      { id: 1, text: 'Do that second' },
      { id: 2, text: 'Do this third' },
      { id: 3, text: 'Do that fourth' },
      { id: 4, text: 'Do this fifth' },
      { id: 5, text: 'Do that sixth' }
    ];
  }
}
