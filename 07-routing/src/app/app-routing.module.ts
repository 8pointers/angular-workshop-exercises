import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HelloWorldComponent} from './hello-world.component';
import { ShoppingCartComponent} from './shopping-cart.component';
import { ClickCounterComponent} from './click-counter.component';
import { TodoComponent} from './todo.component';
import { LoginComponent} from './login.component';

const routes: Routes = [
  { path: '', component: HelloWorldComponent },

  { path: 'helloWorld', component: HelloWorldComponent},
  { path: 'shoppingCart', component: ShoppingCartComponent},
  { path: 'clickCounter', component: ClickCounterComponent},
  { path: 'todo', component: TodoComponent},
  { path: 'login', component: LoginComponent},
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
