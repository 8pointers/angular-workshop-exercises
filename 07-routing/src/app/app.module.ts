import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HelloWorldComponent} from './hello-world.component';
import { ShoppingCartComponent} from './shopping-cart.component';
import { ClickCounterComponent} from './click-counter.component';
import { TodoComponent, TodoItemComponent} from './todo.component';
import { LoginComponent } from './login.component';

@NgModule({
  declarations: [
    AppComponent,
    HelloWorldComponent,
    ShoppingCartComponent,
    ClickCounterComponent,
    TodoComponent,
    TodoItemComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
