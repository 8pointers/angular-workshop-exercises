import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {
  items: any[];

  ngOnInit() {
    this.items = [
      {name: 'Angular 2 Book', price: 10},
      {name: 'Fridge', price: 250},
      {name: 'Socks', price: 20},
      {name: 'AA Batteries', price: 5}
    ];
  }
}
