import { Component, EventEmitter, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-greetings',
  template: `<div *ngFor="let name of names">{{name}}</div>`
})
export class GreetingsComponent implements OnInit {
  @Input()
  names: string[];

  ngOnInit() {
  }
}
