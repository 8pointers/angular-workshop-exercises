import './1-intro.ts';
import './2-creating.ts';
import './3-jasmine.ts';
import './4-delay.ts';
import './5-chaining.ts';
import './6-all.ts';
