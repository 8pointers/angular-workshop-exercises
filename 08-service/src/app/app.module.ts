import { Leaderboard3Component, LeaderboardIndex3Component } from './leaderboard-3';
import { Leaderboard2Component, LeaderboardIndex2Component } from './leaderboard-2';
import { Leaderboard1Component } from './leaderboard-1';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
    Leaderboard1Component,
    Leaderboard2Component,
    Leaderboard3Component,
    LeaderboardIndex2Component,
    LeaderboardIndex3Component
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
