import { Component, Injectable, OnInit } from '@angular/core';
import {Observable} from "rxjs/Rx";
import 'rxjs/add/operator/toPromise';
import { HttpClient, HttpResponse } from '@angular/common/http';

@Injectable()
export class TodoService {
  constructor(private http: HttpClient) {
  }

  getTodosWithFetch(): Promise<any[]> {
    return fetch('data/todo.json')
      .then(response => response.json());
  }

  getTodosWithHttpToPromise(): Promise<any[]> {
    return this.http.get<any[]>('data/todo.json')
      .toPromise();
  }

  getTodosWithHttp(): Observable<any[]> {
    return this.http.get<any[]>('data/todo.json1');
  }
}

@Component({
  selector: 'app-todo',
  providers: [TodoService],
  template: `
    {{reason}}

    <h1>Version 1</h1>
    <div *ngFor="let todo of todos1">{{todo.id}}. {{todo.text}}</div>

    <h2>Version 2</h2>
    <div *ngFor="let todo of todos2 | async">{{todo.id}}. {{todo.text}}</div>

    <h2>Version 3</h2>
    <div *ngFor="let todo of todos3 | async">{{todo.id}}. {{todo.text}}</div>

    <h2>Version 4</h2>
    <div *ngFor="let todo of todos4">{{todo.id}}. {{todo.text}}</div>

    <h2>Version 5</h2>
    <div *ngFor="let todo of todos5 | async">{{todo.id}}. {{todo.text}}</div>
    `
})
export class TodoComponent implements OnInit {
  reason: any;

  todos1: any[];
  todos2: Promise<any[]>;
  todos3: Promise<any[]>;
  todos4: any[];
  todos5: Observable<any[]>;

  constructor(private todoService: TodoService) {
  }

  ngOnInit() {
    this.todoService.getTodosWithFetch()
      .then(todos => this.todos1 = todos, reason => this.reason = reason);
    this.todos2 = this.todoService.getTodosWithFetch();
    this.todos3 = this.todoService.getTodosWithHttpToPromise();
    this.todoService.getTodosWithHttp()
      .subscribe(todos => this.todos4 = todos, reason => this.reason = reason);
    this.todos5 = this.todoService.getTodosWithHttp();
  }
}
