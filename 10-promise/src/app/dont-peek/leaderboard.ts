import { Component, Injectable, OnInit } from '@angular/core';
import {Observable} from "rxjs/Rx";
import 'rxjs/Rx';

import { HttpClient, HttpResponse } from '@angular/common/http';

@Injectable()
export class LeaderboardService {
  constructor(private http: HttpClient) {
  }

  getPlayer(id: number): Promise<any> {
    return fetch(`data/player/${id}.json`)
      .then(response => response.json())
      .then(player => ({id, ...player}));
  }

  getLeaderboard(): Promise<any[]> {
    return fetch('data/leaderboard.json')
      .then(response => response.json());
  }

  getLeaderboardWithPlayers(): Promise<any[]> {
    return this.getLeaderboard()
      .then(leaderboard => leaderboard.map(p => this.getPlayer(p.id)))
      .then(pp => Promise.all(pp));
  }

  getPlayer2(id: number): Observable<any> {
    return this.http.get<any>(`data/player/${id}.json`)
      .map(player => ({id, ...player}));
  }

  getLeaderboard2(): Observable<any[]> {
    return this.http.get<any>('data/leaderboard.json');
  }

  getLeaderboardWithPlayers2(): Observable<any[]> {
    return this.getLeaderboard2()
      .flatMap(leaderboard => Observable.forkJoin(...leaderboard.map(p => this.getPlayer2(p.id))));
  }
}

@Component({
  selector: 'app-leaderboard',
  providers: [LeaderboardService],
  template: `
  <h1>Leaderboard</h1>
  <div *ngFor="let player of players | async; let i = index">{{i + 1}}. {{player.name}} ({{player.id}})</div>
  <h1>Leaderboard2</h1>
  <div *ngFor="let player of players2 | async; let i = index">{{i + 1}}. {{player.name}} ({{player.id}})</div>
`
})
export class LeaderboardComponent implements OnInit {
  players: Promise<any[]>;
  players2: Observable<any[]>;

  constructor(private leaderboardService: LeaderboardService) {
  }

  ngOnInit() {
    this.players = this.leaderboardService.getLeaderboardWithPlayers();
    this.players2 = this.leaderboardService.getLeaderboardWithPlayers2();
  }
}
