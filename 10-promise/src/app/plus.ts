import { Component, Injectable, Input, OnInit } from '@angular/core';

export function plus(delta = 1) {
  return function (target: any, propertyName: string, descriptor: TypedPropertyDescriptor<Function>) {
    const method = descriptor.value;
    descriptor.value = function () {
      const result = method.apply(this, arguments);
      return result + delta;
    };
  };
}

@Injectable()
export class PlusDemoService {
  @plus(2)
  getNumber(n: number) {
    return n;
  }
}

@Component({
  selector: 'app-plus-demo',
  providers: [PlusDemoService],
  template: `<div>
    Input: {{input}} Output: {{output}}
  </div>`
})
export class PlusDemoComponent implements OnInit {
  input = 42;
  output: number;

  constructor(private plusDemoService: PlusDemoService) {
  }

  ngOnInit() {
    this.output = this.plusDemoService.getNumber(this.input);
  }
}
