import { Component, Injectable, OnInit, Pipe, PipeTransform } from '@angular/core';

@Injectable()
export class GameOfLife {
}

@Component({
  providers: [GameOfLife],
  selector: 'app-game-of-life',
  styleUrls: ['./game-of-life.component.css'],
  template: `<div>
    <button class="tick" (click)="tick()">Next</button>
    <div class="grid">
      <div *ngFor="let cell of cells()"
        class="cell"
        [ngClass]="{alive: cell.isAlive}"
        [ngStyle]="{top: 20 * cell.row + 'px', left: 20 * cell.column + 'px'}"
        (click)="toggleCellState(cell.row, cell.column)">
      </div>
    </div>
  </div>`
})
export class GameOfLifeComponent {
  public n = 10;

  constructor(private gameOfLife: GameOfLife) {
  }

  toggleCellState(row, column) {
    this.gameOfLife.toggleCellState(row, column);
  }

  tick() {
    this.gameOfLife.tick();
  }

  cells() {
    return Array.from({length: this.n * this.n})
      .map((element, index) => ({row: Math.floor(index / this.n), column: index % this.n }))
      .map(({row, column}) => ({ row, column, isAlive: this.gameOfLife.isCellAlive(row, column)}));
  }
}
