import { Injectable } from '@angular/core';

@Injectable()
export class GameOfLife {
  public isAlive = {};

  private cellKey(row, column) {
    return row + '_' + column;
  }

  public isCellAlive (row, column) {
    return this.isAlive[this.cellKey(row, column)] || false;
  }

  public toggleCellState (row, column) {
    const key = this.cellKey(row, column);
    if (this.isAlive[key]) {
      delete this.isAlive[key];
    } else {
      this.isAlive[key] = true;
    }
    return this;
  }

  public tick () {
    const numberOfNeighbours = {};
    for (const key in this.isAlive) {
      const [row, column] = key.split('_').map(p => parseInt(p, 10));
      numberOfNeighbours[key] = numberOfNeighbours[key] || 0;
      [[-1, -1], [-1, 0], [-1, 1], [0, -1], [0, 1], [1, -1], [1, 0], [1, 1]].forEach(offset => {
        const neighbourKey = this.cellKey(row + offset[0], column + offset[1]);
        numberOfNeighbours[neighbourKey] = (numberOfNeighbours[neighbourKey] || 0) + 1;
      });
    }
    for (const key in numberOfNeighbours) {
      if (numberOfNeighbours.hasOwnProperty(key)) {
        const shouldDie = this.isAlive[key] && (numberOfNeighbours[key] < 2 || numberOfNeighbours[key] > 3);
        const shouldResurrect = !this.isAlive[key] && numberOfNeighbours[key] === 3;
        if (shouldDie || shouldResurrect) {
          const [row, column] = key.split('_').map(p => parseInt(p, 10));
          this.toggleCellState(row, column);
        }
      }
    }
  };
}
