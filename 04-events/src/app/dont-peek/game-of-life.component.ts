import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-game-of-life',
  styleUrls: ['./game-of-life.component.css'],
  template: `<div>
    <div [ngStyle]="{width: n * width + 'px', height: n * height + 'px'}">
      <div *ngFor="let cell of getCells()"
        class="cell"
        [ngClass]="{alive: cell.isAlive}"
        [ngStyle]="{
          top: height * cell.row + 'px',
          left: height * cell.column + 'px',
          width: width + 'px',
          height: height + 'px'
        }"
        (click)="toggleCellState(cell.row, cell.column)">
      </div>
    </div>
    <button class="tick" (click)="tick()">Next</button>
  </div>`
})
export class GameOfLifeComponent implements OnInit {
  public n = 10;
  public width = 20;
  public height = 20;
  public isAlive = {};

  private cellKey(row, column) {
    return row + '_' + column;
  }

  public isCellAlive (row, column) {
    return this.isAlive[this.cellKey(row, column)] || false;
  }

  public getCells() {
    const n = 10;
    return Array.from(
      {length: n * n},
      (value, index) => {
        const row = Math.floor(index / n);
        const column = index % n;
        return { row, column, isAlive: this.isCellAlive(row, column) };
      }
    );
  }

  public toggleCellState (row, column) {
    const key = this.cellKey(row, column);
    if (this.isAlive[key]) {
      delete this.isAlive[key];
    } else {
      this.isAlive[key] = true;
    }
    return this;
  }

  public tick () {
    const numberOfNeighbours = {};
    let parts, row, column, neighbourKey;
    for (const key in this.isAlive) {
      if (this.isAlive.hasOwnProperty(key)) {
        parts = key.split('_');
        row = parseInt(parts[0], 10);
        column = parseInt(parts[1], 10);
        numberOfNeighbours[key] = numberOfNeighbours[key] || 0;
        [[-1, -1], [-1, 0], [-1, 1], [0, -1], [0, 1], [1, -1], [1, 0], [1, 1]].forEach(offset => {
          neighbourKey = this.cellKey(row + offset[0], column + offset[1]);
          numberOfNeighbours[neighbourKey] = (numberOfNeighbours[neighbourKey] || 0) + 1;
        });
      }
    }
    for (const key in numberOfNeighbours) {
      if (numberOfNeighbours.hasOwnProperty(key)) {
        const shouldDie = this.isAlive[key] && (numberOfNeighbours[key] < 2 || numberOfNeighbours[key] > 3);
        const shouldResurrect = !this.isAlive[key] && numberOfNeighbours[key] === 3;
        if (shouldDie || shouldResurrect) {
          parts = key.split('_');
          row = parseInt(parts[0], 10);
          column = parseInt(parts[1], 10);
          this.toggleCellState(row, column);
        }
      }
    }
  };

  ngOnInit() {
  }
}
