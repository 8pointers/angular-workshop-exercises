import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-showcase',
  template: `
    <span [ngClass]="{tall: person.isTall}">
      With Object
    </span>
    <div [ngClass]="['first', second]">
      With Array
    </div>
    <div [ngClass]="'first second'">
      With String
    </div>`
})
export class ShowcaseComponent {
  person = {isTall: true};
  second = 'secondClass';
}
